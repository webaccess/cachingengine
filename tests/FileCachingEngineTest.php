<?php

class FileCachingEngineTest extends PHPUnit_Framework_TestCase
{

    public function testConstruct()
    {
        $cachingEngine = new \CachingEngine\FileCachingEngine();

        $this->assertInstanceOf('\CachingEngine\FileCachingEngine', $cachingEngine);
    }

    public function testSet()
    {
        $storageEngine = new \StorageEngine\FileEngine();
        $cachingEngine = new \CachingEngine\FileCachingEngine($this->_getCacheFolder(), $storageEngine);
        $data = array(1, 2, 3);
        $cachingEngine->set('data', $data);

        $this->assertFileExists($this->_getCacheFolder() . 'data.html');
    }

    public function testSet2()
    {
        $this->setExpectedException('\Exception');
        $storageEngine = new \StorageEngine\FileEngine();
        $cachingEngine = new \CachingEngine\FileCachingEngine('/unwritableFolder', $storageEngine);
        $data = array(1, 2, 3);
        $cachingEngine->set('data', $data);

        $this->assertEquals($data, $cachingEngine->get('data'));
    }

    public function testGet()
    {
        $storageEngine = new \StorageEngine\FileEngine();
        $cachingEngine = new \CachingEngine\FileCachingEngine($this->_getCacheFolder(), $storageEngine);
        $data = array(1, 2, 3);

        $this->assertEquals($data, $cachingEngine->get('data'));
    }

    public function testGet2()
    {
        $storageEngine = new \StorageEngine\FileEngine();
        $cachingEngine = new \CachingEngine\FileCachingEngine($this->_getCacheFolder(), $storageEngine);
        $data = array(1, 2, 3);

        $this->assertEquals($data, $cachingEngine->get('data'));
    }

    public function testGetNonExistent()
    {
        $storageEngine = new \StorageEngine\FileEngine();
        $cachingEngine = new \CachingEngine\FileCachingEngine($this->_getCacheFolder(), $storageEngine);

        $this->assertEquals(false, $cachingEngine->get('data3'));
    }

    public function testGet3()
    {
        $storageEngine = new \StorageEngine\FileEngine();
        $cachingEngine = new \CachingEngine\FileCachingEngine($this->_getCacheFolder(), $storageEngine);
        $data = array(1, 2, 3);

        $this->assertEquals(false, $cachingEngine->get('expired-data'));
    }

    public function testDelete()
    {
        $storageEngine = new \StorageEngine\FileEngine();
        $cachingEngine = new \CachingEngine\FileCachingEngine($this->_getCacheFolder(), $storageEngine);
        $data = array(1, 2, 3);

        $this->assertFileExists($this->_getCacheFolder() . 'data.html');
        $cachingEngine->delete('data');
        
        $this->assertFalse(file_exists($this->_getCacheFolder() . 'data.html'));
    }

    public function testGetIdentifier()
    {
        $storageEngine = new \StorageEngine\FileEngine();
        $cachingEngine = new \CachingEngine\FileCachingEngine($this->_getCacheFolder(), $storageEngine);
        
        $file = '/templates/template.html';
        $arguments = array('items' => array(1, 2, 3));
        $identifierExpected = 'dGVtcGxhdGVzdGVtcGxhdGUuaHRtbGE6MTp7czo1OiJpdGVtcyI7YTozOntpOjA7aToxO2k6MTtpOjI7aToyO2k6Mzt9fQ==';
        
        $this->assertEquals($identifierExpected, $cachingEngine->getIdentifier($file, $arguments));
    }

    private function _getCacheFolder()
    {
        return __DIR__ . '/files/';
    }
}