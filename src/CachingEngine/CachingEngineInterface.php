<?php

namespace CachingEngine;

interface CachingEngineInterface {

    public function get($identifier);

    public function set($identifier, $object, $duration = 600);

    public function delete($identifier);
}