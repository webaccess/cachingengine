<?php

namespace CachingEngine;

class FileCachingEngine implements CachingEngineInterface {

    private $_path;
    private $_extension;
    private $_storageEngine;

    const DEFAULT_EXPIRATION = 600;
    const DEFAULT_EXTENSION = '.html';

    public function __construct($path = '', $storageEngine = null)
    {
        $this->_path = $path;
        $this->_storageEngine = $storageEngine;
        $this->_extension = self::DEFAULT_EXTENSION;
    }

    public function get($identifier)
    {
        $file = $this->_getCacheFile($identifier);
        if ($this->_storageEngine->isExistent($file)) {
            $data = $this->_decode($this->_storageEngine->get($file));

            $expirationTimestamp = $data['expiration_timestamp'];
            $data = $data['data'];

            $expirationDate = new \DateTime();
            $currentDate = new \DateTime();
            $expirationDate->setTimestamp($expirationTimestamp);

            if ($currentDate > $expirationDate) {
                return false;
            }

            return $data;
        }
        return false;
    }

    public function set($identifier, $data, $duration = self::DEFAULT_EXPIRATION)
    {
        $file = $this->_getCacheFile($identifier);
        $data = array('data' => $data, 'expiration_timestamp' => $this->_getExpirationTimestamp($duration));
        $this->_storageEngine->set($file, $this->_encode($data));
    }

    public function delete($identifier)
    {
        $file = $this->_getCacheFile($identifier);
        if ($this->_storageEngine->isExistent($file)) {
            $this->_storageEngine->delete($file);
        }
    }

    private function _getCacheFile($identifier)
    {
        return $this->_path . $identifier . $this->_extension;
    }

    private function _encode($data)
    {
        return serialize($data);
    }

    private function _decode($data)
    {
        return unserialize($data);
    }

    private function _getExpirationTimestamp($duration)
    {
        $date = new \DateTime();
        $interval = new \DateInterval('PT' . $duration . 'S');
        $date->add($interval);
        return $date->getTimestamp();
    }

    public function getIdentifier($file, $arguments = array())
    {
        $identifier = str_replace('/', '', $file);
        $identifier .= serialize($arguments);
        return base64_encode($identifier);
    }

}